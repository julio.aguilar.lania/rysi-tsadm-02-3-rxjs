import { Subject } from "rxjs";

const tema1 = new Subject({});
const watcher_1 = tema1.subscribe(x => { console.log("W1", x);});
const watcher_2 = tema1.subscribe(x => { console.log("W2", x);});

tema1.next(123);
tema1.next("Hola rxjs");

