import { from, interval } from 'rxjs';
import { take, filter, skip, toArray } from 'rxjs/operators'

const obsv_arreglo = from(['h','o','l','a']);
const obsv_intervalo = interval(500);

const watcher_1 = obsv_arreglo.subscribe(x => { console.log("W1", x);});
const watcher_2 = obsv_intervalo.subscribe(x => { console.log("W2", x);});
const watcher_3 = obsv_intervalo.pipe(take(3))
                    .subscribe(x => {console.log("W-take", x);});
const watcher_4 = obsv_intervalo.pipe(filter(x => x % 3 === 0))
                    .subscribe(x => {console.log("W-filter", x);});
const watcher_5 = obsv_intervalo.pipe(skip(3))
                    .subscribe(x => {console.log("W-skip", x);});
/*
SINTAXIS VIEJA
const watcher_6 = obsv_intervalo
                    .take(7)
                    .toArray()
                    .subscribe(x => {console.log("W-toArray", x);});
                    */
const watcher_6 = obsv_intervalo.pipe(take(7), toArray())
                    .subscribe(x => {console.log("W-toArray", x);});