import { Observable } from 'rxjs';

const obsv_uno = new Observable(observador => {
    observador.next(1);
    observador.next(5);
    observador.next(7);
    setTimeout(() => {observador.next(11)}, 5000);
    setTimeout(() => {observador.next(30)}, 1000);
    observador.next(20);
    //observador.error("ERROR esperado");
    observador.complete();
    observador.next(100);
});


const watcher_completo = {
    next: x => { console.log("WATCHER3", x)},
    error: err => {console.log("WATCHER3 ERROR:", err)},
    complete: () => { console.log("WATCHER3 notificado de COMPLETE")}
}

const watcher_1 = obsv_uno.subscribe(x => { console.log("W1", x);}, err=> {});
const watcher_2 = obsv_uno.subscribe(x => { console.log("W2", x*x)});
// const watcher_2 = obsv_uno.pipe(map(x => x*x)).subscribe(x => { console.log("W2", x)});
const watcher_3 = obsv_uno.subscribe(watcher_completo);